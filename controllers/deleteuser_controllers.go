package controllers

import (
	"fibersample/models"
	"fibersample/repo"

	"github.com/gofiber/fiber/v2"
)

func DeleteUsers(c *fiber.Ctx) error {

	id := c.Params("id")
	db := repo.Connectionpool
	var user models.Users
	err := db.Find(&user, id).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "massage": "Could not find todo", "data": err})
	}

	db.Delete(&user)

	return c.Status(200).JSON(map[string]interface{}{

		"message": "ลบเรียบร้อย",
	})
}
