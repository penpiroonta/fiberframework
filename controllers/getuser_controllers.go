package controllers

import (
	"fibersample/repo"

	"github.com/gofiber/fiber/v2"
)

func GetUsersControllers(c *fiber.Ctx) error {

	result, err := repo.GetUserRepo()

	if err != nil {
		return c.Status(500).JSON(&fiber.Map{
			"success": false,
			"data":    nil,
		})
	}

	return c.Status(200).JSON(&fiber.Map{
		"data": result,
	})
}
