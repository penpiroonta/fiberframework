package controllers

import (
	"fibersample/repo"

	"github.com/gofiber/fiber/v2"
)

func UpdateUser(c *fiber.Ctx) error {

	type Users struct {
		ID        int    `json:"id"`
		Username  string `json:"username"`
		Password  string `json:"password"`
		Email     string `json:"email"`
		Phone     string `json:"phone"`
		Address   string `json:"address"`
		User_type string `json:"user_type"`
	}

	id := c.Params("id")
	db := repo.Connectionpool

	var updatedUsers Users
	err := c.BodyParser(&updatedUsers)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "massage": "Review your input users", "data": err})
	}

	var users Users
	err = db.Find(&users, id).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "massage": "Could not find users", "data": err})
	}

	users.ID = updatedUsers.ID
	users.Username = updatedUsers.Username
	users.Password = updatedUsers.Password
	users.Email = updatedUsers.Email
	users.Phone = updatedUsers.Phone
	users.Address = updatedUsers.Address
	users.User_type = updatedUsers.User_type

	db.Debug().Save(&users)
	return c.Status(200).JSON(map[string]interface{}{
		"data":    &users,
		"message": "xxxx",
	})
}
