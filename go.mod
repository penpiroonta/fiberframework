module fibersample

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.41.0
	gorm.io/driver/mysql v1.4.5
	gorm.io/gorm v1.24.3
)
