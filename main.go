package main

import (
	"fibersample/controllers"
	"fibersample/repo"

	"github.com/gofiber/fiber/v2"
)

func main() {

	repo.InitailDB()
	db, _ := repo.Connectionpool.DB()
	defer db.Close()

	app := fiber.New()

	app.Get("/users", controllers.GetUsersControllers)
	app.Put("/users/:id", controllers.UpdateUser)
	app.Post("/users", controllers.CreateUser)
	app.Delete("/users/:id", controllers.DeleteUsers)
	app.Listen(":3000")
}
