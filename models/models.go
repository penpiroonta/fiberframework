package models

import "time"

type Users struct {
	ID        int    `json:"id"`
	Username  string `json:"username"`
	Password  string `json:"password"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
	Address   string `json:"address"`
	User_type string `json:"user_type"`
}

type Orders struct {
	OrdersID     int       `json:"id"`
	OrderProduct string    `json:"order_product"`
	OrderDate    time.Time `json:"order_date"`
	UserId       int       `json:"user_id"`
}

type OrderDetails struct {
	OrderDetailsID int     `json:"id"`
	OrderId        int     `json:"order_id"`
	ProductId      int     `json:"product_id"`
	Amount         int     `json:"amount"`
	Coat           float64 `json:"coat"`
}

type Product struct {
	ProductID     int     `json:"id"`
	ProductName   string  `json:"product_name"`
	ProductPrice  float64 `json:"product_price"`
	ProductAmount int     `json:"product_amount"`
	UserId        int     `json:"user_id"`
}

// Resiver function

func (u Users) TableName() string {
	return "users"

}
func (o Orders) TableName() string {
	return "orders"

}
func (or OrderDetails) TableName() string {
	return "orders_details"

}
func (p Product) TableName() string {
	return "poduct"

}
