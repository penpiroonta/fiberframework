package repo

import "fibersample/models"

func GetUserRepo() ([]models.Users, error) {

	users := new([]models.Users)

	err := Connectionpool.Find(&users).Error
	return *users, err

}
